const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const {authMiddleware} = require('./middleware/authMiddleware');


const {authRouter} = require('./authRouter.js');
const {usersRouter} = require('./usersRouter.js');
const DB_URL = process.env.DB_URL;
const PORT = process.env.PORT;

const app = express();
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', authMiddleware, usersRouter);



const start = async () => {
  try {
    await mongoose.connect(DB_URL, {
      useNewUrlParser: true,
    })
    app.listen(PORT, () => {
      console.log('Server has been  start')
    });
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};
start();
// app.use('/', (rec, res)=> {
//   res.send('<h1>Express start</h1>')
// });

