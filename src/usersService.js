const { User } = require('./models/User.js');
const bcrypt = require('bcryptjs');

const getUser = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.userId);
    const {id, username, createdAt} = user;
    return res.json({
      'user': {
        '_id': id,
        username,
        'createdDate': createdAt,
      },
    });
  } catch (e) {
    return res.status(500).json({
      'message': e.message,
    });
  }
};

const deleteUser = async (req, res, next) => {
  try {
    await User.findByIdAndDelete(req.user.userId);
    return res.json({
      'message': 'Success',
    });
  } catch (e) {
    return res.status(500).json({
      'message': e.message,
    });
  }
};

const changePassword = async (req, res) => {
  const user = await User.findById(req.userProfile.userId);
  const match = await bcrypt
      .compare(String(req.body.oldPassword), String(user.password));

  if (!match) {
    return res.status(400).json({
      message: 'Please enter correct current password',
    });
  }
  try {
    user.password = await bcrypt.hash(req.body.newPassword, 10);
    user.save();

    return res.status(200).json({message: 'Success'});
  } catch (err) {
    throw err;
  }
};



module.exports = {
  getUser,
  deleteUser,
  changePassword,
};
